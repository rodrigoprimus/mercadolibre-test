# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

###### models
-keep class com.example.mercadolibresearch.models.** { *; }

###### eventbus
-keepattributes *Annotation*
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

###### retrofit
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions

-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}

###### okhttp
-dontwarn okio.**
-dontwarn com.squareup.**
-dontwarn okhttp3.**
-keep class okhttp3.** { *; }
-keep class com.squareup.** { *; }
-keep interface com.squareup.** { *; }

###### android databinding (to work with minifyEnabled=true, useProguard=false aka Android Shrinker)
-keep class androidx.databinding.** {
   *;
}
-dontobfuscate
