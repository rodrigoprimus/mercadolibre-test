package com.example.mercadolibresearch;

import android.app.Application;

import com.example.mercadolibresearch.api.MercadoLivreClient;

/**
 * Created by rodrigoprimus on 08/06/19.
 */

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        MercadoLivreClient mercadoLivreClient = new MercadoLivreClient(this, "https://api.mercadolibre.com/");
        mercadoLivreClient.setup();
    }
}
