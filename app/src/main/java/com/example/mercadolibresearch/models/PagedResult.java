package com.example.mercadolibresearch.models;

import java.util.List;

/**
 * Created by rodrigoprimus on 08/06/19.
 */

public class PagedResult<T> {

    List<T> results;

    public List<T> getResults() {
        return results;
    }

    public void setResults(List<T> results) {
        this.results = results;
    }
}
