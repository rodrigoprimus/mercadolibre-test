package com.example.mercadolibresearch.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by rodrigoprimus on 08/06/19.
 */

public class Product implements Parcelable {

    private String id;
    private String title;
    private String thumbnail;
    private Float price;
    private Long sold_quantity;

    private List<Pictures> pictures;

    protected Product(Parcel in) {
        id = in.readString();
        title = in.readString();
        thumbnail = in.readString();
        float _price = in.readFloat();
        price = _price == Float.MAX_VALUE ? null : _price;
        sold_quantity = in.readLong();
        pictures = in.createTypedArrayList(Pictures.CREATOR);
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(thumbnail);
        dest.writeFloat(price == null ? Float.MAX_VALUE : price);
        dest.writeLong(sold_quantity);
        dest.writeTypedList(pictures);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Long getSold_quantity() {
        return sold_quantity;
    }

    public void setSold_quantity(Long sold_quantity) {
        this.sold_quantity = sold_quantity;
    }

    public List<Pictures> getPictures() {
        return pictures;
    }

    public void setPictures(List<Pictures> pictures) {
        this.pictures = pictures;
    }
}
