package com.example.mercadolibresearch.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by rodrigoprimus on 11/06/19.
 */

public class Pictures implements Parcelable {

    private String id;
    private String secure_url;

    protected Pictures(Parcel in) {
        id = in.readString();
        secure_url = in.readString();
    }

    public static final Creator<Pictures> CREATOR = new Creator<Pictures>() {
        @Override
        public Pictures createFromParcel(Parcel in) {
            return new Pictures(in);
        }

        @Override
        public Pictures[] newArray(int size) {
            return new Pictures[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(secure_url);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSecure_url() {
        return secure_url;
    }

    public void setSecure_url(String secure_url) {
        this.secure_url = secure_url;
    }
}
