package com.example.mercadolibresearch.api;

import android.content.Context;

import com.example.mercadolibresearch.models.PagedResult;
import com.example.mercadolibresearch.models.Product;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import br.com.rodrigoprimus.customlibrary.service.events.ErrorEvent;
import br.com.rodrigoprimus.customlibrary.service.events.ResponseEvent;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rodrigoprimus on 08/06/19.
 */

public class MercadoLivreService {

    protected final Context context;

    protected MercadoLivreApi api;

    protected EventBus bus;

    public MercadoLivreService(Context context, MercadoLivreApi api, EventBus bus) {
        this.context = context;
        this.api = api;
        this.bus = bus;
        this.bus.register(this);
    }

    @Subscribe
    public void onEvent(final SearchProductEvent event) {
        api.search(event.query).enqueue(new Callback<PagedResult<Product>>() {
            @Override
            public void onResponse(Call<PagedResult<Product>> call, Response<PagedResult<Product>> response) {
                switch (response.code()) {
                    case 200:
                        bus.post(new SearchProductLoadedEvent(event, response.body()));
                        break;
                    default:
                        bus.post(new SearchProductEventErrorEvent(event, response, null));
                        break;
                }
            }

            @Override
            public void onFailure(Call<PagedResult<Product>> call, Throwable t) {
                bus.post(new SearchProductEventErrorEvent(event, null, t));
            }
        });
    }

    @Subscribe
    public void onEvent(final LoadProductsEvent event) {
        api.searchBy(event.category, event.storeId).enqueue(new Callback<PagedResult<Product>>() {
            @Override
            public void onResponse(Call<PagedResult<Product>> call, Response<PagedResult<Product>> response) {
                switch (response.code()) {
                    case 200:
                        bus.post(new ProductsLoadedEvent(event, response.body()));
                        break;
                    default:
                        bus.post(new LoadProductsErrorEvent(event, response, null));
                        break;
                }
            }

            @Override
            public void onFailure(Call<PagedResult<Product>> call, Throwable t) {
                bus.post(new LoadProductsErrorEvent(event, null, t));
            }
        });
    }

    @Subscribe
    public void onEvent(final LoadProductDetailEvent event) {
        api.detail(event.id).enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                switch (response.code()) {
                    case 200:
                        bus.post(new ProductDetailLoadedEvent(event, response.body()));
                        break;
                    default:
                        bus.post(new LoadProductDetailErrorEvent(event, response, null));
                        break;
                }
            }

            @Override
            public void onFailure(Call<Product> call, Throwable t) {
                bus.post(new LoadProductDetailErrorEvent(event, null, t));
            }
        });
    }

    public static class SearchProductEvent {

        public final String query;

        public SearchProductEvent(String query) {
            this.query = query;
        }
    }

    public static class LoadProductsEvent {
        public final String category;
        public final String storeId;

        public LoadProductsEvent(String category, String storeId) {
            this.category = category;
            this.storeId = storeId;
        }
    }

    public static class LoadProductDetailEvent {
        public final String id;

        public LoadProductDetailEvent(String id) {
            this.id = id;
        }
    }


    // RESPONSE EVENTS

    public static class SearchProductLoadedEvent extends ResponseEvent<SearchProductEvent> {

        public final PagedResult<Product> product;

        public SearchProductLoadedEvent(SearchProductEvent event, PagedResult<Product> product) {
            super(event);
            this.product = product;
        }
    }

    public static class ProductsLoadedEvent extends ResponseEvent<LoadProductsEvent> {

        public final PagedResult<Product> product;

        public ProductsLoadedEvent(LoadProductsEvent event, PagedResult<Product> product) {
            super(event);
            this.product = product;
        }
    }

    public static class ProductDetailLoadedEvent extends ResponseEvent<LoadProductDetailEvent> {

        public final Product product;

        public ProductDetailLoadedEvent(LoadProductDetailEvent event, Product product) {
            super(event);
            this.product = product;
        }
    }

    // ERROR EVENTS

    public static class SearchProductEventErrorEvent extends ErrorEvent<SearchProductEvent, PagedResult<Product>> {

        public SearchProductEventErrorEvent(SearchProductEvent source, Response<PagedResult<Product>> response, Throwable throwable) {
            super(source, response, throwable);
        }
    }

    public static class LoadProductsErrorEvent extends ErrorEvent<LoadProductsEvent, PagedResult<Product>> {

        public LoadProductsErrorEvent(LoadProductsEvent source, Response<PagedResult<Product>> response, Throwable throwable) {
            super(source, response, throwable);
        }
    }

    public static class LoadProductDetailErrorEvent extends ErrorEvent<LoadProductDetailEvent, Product> {
        public LoadProductDetailErrorEvent(LoadProductDetailEvent source, Response<Product> response, Throwable throwable) {
            super(source, response, throwable);
        }
    }

}
