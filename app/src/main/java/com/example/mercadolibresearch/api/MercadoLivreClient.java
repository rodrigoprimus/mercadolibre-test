package com.example.mercadolibresearch.api;

import android.content.Context;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rodrigoprimus on 08/06/19.
 */

public class MercadoLivreClient {

    private static final int TIMEOUT_IN_SECONDS = 10;

    protected Retrofit retrofit;
    protected Context context;
    protected MercadoLivreService mercadoLivreService;

    public MercadoLivreClient(Context context, String baseUrl) {
        this.context = context;

        OkHttpClient client = createHttpClient(context);
        GsonConverterFactory gsonFactory = createGsonFactory();

        retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(baseUrl)
                .addConverterFactory(gsonFactory)
                .build();
    }

    public static GsonConverterFactory createGsonFactory() {
        GsonBuilder builder = new GsonBuilder();
        builder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).serializeNulls().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        return GsonConverterFactory.create(builder.create());
    }

    protected OkHttpClient createHttpClient(final Context context) {

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
                .build();

        return client;
    }

    public void setup() {
        // create services instances
        getMercadoLivreService();
    }

    public MercadoLivreService getMercadoLivreService() {
        if (mercadoLivreService == null) {
            MercadoLivreApi api = retrofit.create(MercadoLivreApi.class);
            mercadoLivreService = new MercadoLivreService(context, api, EventBus.getDefault());
        }
        return mercadoLivreService;
    }
}
