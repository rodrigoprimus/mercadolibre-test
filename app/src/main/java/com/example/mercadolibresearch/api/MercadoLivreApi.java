package com.example.mercadolibresearch.api;

import com.example.mercadolibresearch.models.PagedResult;
import com.example.mercadolibresearch.models.Product;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by rodrigoprimus on 08/06/19.
 */

public interface MercadoLivreApi {

    @GET("sites/MLB/search")
    Call<PagedResult<Product>> search(@Query("q") String q);

    @GET("sites/MLB/search")
    Call<PagedResult<Product>> searchBy(@Query("category") String category, @Query("official_store_id") String storeId);

    @GET("items/{id}")
    Call<Product> detail(@Path("id") String id);

}

