package com.example.mercadolibresearch.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.example.mercadolibresearch.R;
import com.example.mercadolibresearch.databinding.ComponentSearchBinding;

import java.util.Locale;

import br.com.rodrigoprimus.customlibrary.utils.DelayedHelper;

/**
 * Created by rodrigoprimus on 08/06/19.
 */

public class SearchProductView extends FrameLayout {

    private ComponentSearchBinding binding;

    private TextEditControl inputProduct;

    private SearchListener searchListener;

    private boolean preventSearchListenerCallback;

    public SearchProductView(@NonNull Context context) {
        this(context, null, 0);
    }

    public SearchProductView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SearchProductView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if (isInEditMode()) {
            LayoutInflater.from(context).inflate(R.layout.component_search, this, true);
        } else {
            binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.component_search, this, true);
        }

        inputProduct = binding.inputStreet;
        inputProduct.setListener(new TextEditControl.TextEditControlListener() {
            @Override
            public void onBackPressed() {
                if (searchListener != null) {
                    searchListener.backButtonClick(true);
                }
            }
        });

        setupEditText(inputProduct, true);

        inputProduct.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    inputProduct.clearFocus();
                    submitSearchProduct();
                }
                return false;
            }
        });

        binding.backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchListener != null) {
                    searchListener.backButtonClick(false);
                }
            }
        });

        binding.inputStreet.requestFocus();
    }

    private void submitTextProduct() {
        if (searchListener != null) {
            String inputText = inputProduct.getText().toString();
            String query = String.format(Locale.getDefault(), "%s", inputText);
            searchListener.requestProduct(query);
        }
    }

    private void submitSearchProduct() {
        if (searchListener != null) {
            String productQuery = inputProduct.getText().toString();
            String product = String.format(Locale.getDefault(), "%s", productQuery);
            searchListener.submitProduct(product);
        }
    }

    public void setProductValue(CharSequence product) {
        preventSearchListenerCallback = true;

        inputProduct.setText(product);

        preventSearchListenerCallback = false;
    }

    private void setupEditText(final EditText editText, final boolean withCloseButton) {

        final Drawable closeIcon = ContextCompat.getDrawable(getContext(), R.drawable.ic_cancel_gray);

        editText.addTextChangedListener(new TextWatcher() {

            Handler requestProductHandler;
            Runnable requestProductRunnable = new Runnable() {
                @Override
                public void run() {
                    submitTextProduct();
                }
            };

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                editText.setCompoundDrawablesWithIntrinsicBounds(null, null, withCloseButton ? closeIcon : null, null);

                if (editText.isEnabled() && !preventSearchListenerCallback) {
                    DelayedHelper.cancelHandler(requestProductHandler);
                    requestProductHandler = DelayedHelper.postCancelableDelayed((Activity) getContext(), requestProductRunnable, 300);
                }
            }
        });

        editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (searchListener != null) {
                        searchListener.onInputTouch((String) editText.getTag());
                    }
                    Drawable compoundDrawable = editText.getCompoundDrawables()[DRAWABLE_RIGHT];
                    if (compoundDrawable != null) {
                        if (event.getRawX() >= (editText.getRight() - editText.getTotalPaddingRight())) {
                            if (editText.getText().toString().length() > 0) {
                                clearInputText();
                            }

                            performClick();
                            return true;
                        }
                    }
                }
                return false;
            }
        });
    }

    public AppCompatEditText getInputText() {
        return binding.inputStreet;
    }

    public void clearInputText() {
        inputProduct.setText("");
        inputProduct.requestFocus();

        if (searchListener != null) {
            searchListener.clearSearch();
        }
    }

    public boolean inputProductIsEmpty() {
        return inputProduct.getText().toString().isEmpty();
    }

    public void setListener(SearchListener productViewListener) {
        searchListener = productViewListener;
    }

    public interface SearchListener {
        void clearSearch();

        void requestProduct(String query);

        void submitProduct(String product);

        void onInputTouch(String prefix);

        void backButtonClick(boolean hideKeyboard);
    }
}
