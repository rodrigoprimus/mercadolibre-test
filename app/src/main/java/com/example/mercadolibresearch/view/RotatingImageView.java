package com.example.mercadolibresearch.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

/**
 * Created by rodrigoprimus on 10/06/19.
 */

public class RotatingImageView extends AppCompatImageView {

    private RotateAnimation currentAnimation;

    public RotatingImageView(Context context) {
        this(context, null, 0);
    }

    public RotatingImageView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RotatingImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        rotate();
    }

    private void rotate(){
        if (getVisibility() != VISIBLE) return;

        if ( currentAnimation != null ) {
            currentAnimation.cancel();
        }

        currentAnimation = new RotateAnimation(0,360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f    );
        currentAnimation.setDuration(800);
        currentAnimation.setInterpolator(new LinearInterpolator());
        currentAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                rotate();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        clearAnimation();
        startAnimation(currentAnimation);
    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if( visibility == View.VISIBLE ){
            rotate();
        } else {
            if ( currentAnimation != null ) {
                currentAnimation.cancel();
                currentAnimation = null;
            }
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        rotate();
    }
}
