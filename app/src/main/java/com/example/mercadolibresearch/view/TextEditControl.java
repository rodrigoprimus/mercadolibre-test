package com.example.mercadolibresearch.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;

import androidx.appcompat.widget.AppCompatEditText;

import com.example.mercadolibresearch.R;

/**
 * Created by rodrigoprimus on 08/06/19.
 */

public class TextEditControl extends AppCompatEditText {

    private TextEditControlListener textEditControlListener;

    public TextEditControl(Context context) {
        super(context, null);
    }

    public TextEditControl(Context context, AttributeSet attrs) {
        super(context, attrs, R.attr.editTextStyle);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK && textEditControlListener != null){
            textEditControlListener.onBackPressed();
            return true;
        }
        else{
            return super.dispatchKeyEvent(event);
        }
    }

    public void setListener(TextEditControlListener listener) {
        textEditControlListener = listener;
    }

    public interface TextEditControlListener {
        void onBackPressed();
    }

}
