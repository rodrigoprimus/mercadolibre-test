package com.example.mercadolibresearch.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

import androidx.databinding.DataBindingUtil;

import com.example.mercadolibresearch.R;
import com.example.mercadolibresearch.databinding.ComponentLoadingBinding;

/**
 * Created by rodrigoprimus on 10/06/19.
 */

public class LoadingView extends FrameLayout {

    private ComponentLoadingBinding viewBinding;

    public LoadingView(Context context) {
        super(context);
        init(context, null);
    }

    public LoadingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public LoadingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        viewBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.component_loading, this, true);

        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    public void show() {
        if (getVisibility() != VISIBLE) {
            setVisibility(VISIBLE);
        }
    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
    }

    public void hide() {
        if (getVisibility() != GONE) {
            setVisibility(GONE);
        }
    }
}
