package com.example.mercadolibresearch.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import com.example.mercadolibresearch.R;
import com.example.mercadolibresearch.api.MercadoLivreService;
import com.example.mercadolibresearch.databinding.ActivityMainBinding;
import com.example.mercadolibresearch.models.Product;
import com.example.mercadolibresearch.utils.DimensionsHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import br.com.rodrigoprimus.customlibrary.adapter.BindingRecyclerViewArrayAdapter;
import br.com.rodrigoprimus.customlibrary.adapter.RecyclerViewArrayAdapter;
import br.com.rodrigoprimus.customlibrary.adapter.decoration.ItemOffsetDecorator;

/**
 * Created by rodrigoprimus on 08/06/19.
 */

public class MainActivity extends BaseActivity {

    private ActivityMainBinding binding;

    private MercadoLivreService.LoadProductsEvent loadProductsEvent;
    private BindingRecyclerViewArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        binding.title.setText("Mercado Livre");
        binding.toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SearchProductActivity.class);
                startActivityForResult(intent, SEARCH_PRODUCT);
            }
        });

        setShouldRegisterForEvents(true);

        binding.loadingView.show();
        loadProductsEvent = new MercadoLivreService.LoadProductsEvent("MLB1574", "all");
        EventBus.getDefault().post(loadProductsEvent);


        adapter = new BindingRecyclerViewArrayAdapter<Product>(this, R.layout.item_product, com.example.mercadolibresearch.BR.product);
        adapter.setOnItemClickListener(new RecyclerViewArrayAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(View view, Object o, int i) {
                Intent intent = new Intent(MainActivity.this, ProductDetailActivity.class);
                Product product = (Product) o;
                intent.putExtra(PRODUCT_ID, product.getId());
                startActivity(intent);
            }
        });

        int screenSize = DimensionsHelper.getScreenSize(this).x;
        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            // In landscape
            screenSize = DimensionsHelper.getScreenSize(this).y;
        }
        int gridSize = screenSize / (int) DimensionsHelper.dpToPixels(160);
        GridLayoutManager layoutManager = new GridLayoutManager(this, gridSize);

        int spacing = (int) getResources().getDimension(R.dimen.list_item_spacing);
        ItemOffsetDecorator decoration = new ItemOffsetDecorator(spacing, spacing, 0, spacing, false);
        binding.recycler.addItemDecoration(decoration);
        binding.recycler.setLayoutManager(layoutManager);
        binding.recycler.setAdapter(adapter);
    }

    @Subscribe
    public void onEvent(MercadoLivreService.ProductsLoadedEvent event) {
        binding.loadingView.hide();
        if (event.source == loadProductsEvent) {
            if (event.product != null) {
                adapter.bindData(event.product.getResults());
            }
        }
    }

    @Subscribe
    public void onEvent(MercadoLivreService.LoadProductsErrorEvent event) {
        binding.loadingView.hide();
        if (event.source == loadProductsEvent) {
            Log.i("HERE>>", "LoadProductsErrorEvent: " + event);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == SEARCH_PRODUCT) {
            if (resultCode == RESULT_OK) {
                //show result products
            } else {
                //show exception
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
