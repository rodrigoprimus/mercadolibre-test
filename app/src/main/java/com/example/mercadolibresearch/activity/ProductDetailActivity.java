package com.example.mercadolibresearch.activity;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.example.mercadolibresearch.R;
import com.example.mercadolibresearch.api.MercadoLivreService;
import com.example.mercadolibresearch.databinding.ActivityProductDetailBinding;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by rodrigoprimus on 11/06/19.
 */

public class ProductDetailActivity extends BaseActivity {

    private ActivityProductDetailBinding binding;
    private MercadoLivreService.LoadProductDetailEvent loadProductDetailEvent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_product_detail);

        setShouldRegisterForEvents(true);

        setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String productId = getIntent().getStringExtra(PRODUCT_ID);
        if (productId != null) {
            binding.loadingView.show();
            loadProductDetailEvent = new MercadoLivreService.LoadProductDetailEvent(productId);
            EventBus.getDefault().post(loadProductDetailEvent);
        }
    }

    @Subscribe
    public void onEvent(MercadoLivreService.ProductDetailLoadedEvent event) {
        binding.loadingView.hide();
        if (event.source == loadProductDetailEvent) {
            if (event.product != null) {
                binding.setProduct(event.product);
            }
        }
    }

    @Subscribe
    public void onEvent(MercadoLivreService.LoadProductDetailErrorEvent event) {
        binding.loadingView.hide();
        if (event.source == loadProductDetailEvent) {
            Log.i("HERE>>", "LoadProductDetailErrorEvent: " + event);
        }
    }


}
