package com.example.mercadolibresearch.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.mercadolibresearch.BR;
import com.example.mercadolibresearch.R;
import com.example.mercadolibresearch.api.MercadoLivreService;
import com.example.mercadolibresearch.databinding.ActivitySearchProductBinding;
import com.example.mercadolibresearch.models.Product;
import com.example.mercadolibresearch.view.SearchProductView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import br.com.rodrigoprimus.customlibrary.adapter.BindingRecyclerViewArrayAdapter;
import br.com.rodrigoprimus.customlibrary.adapter.RecyclerViewArrayAdapter;


/**
 * Created by rodrigoprimus on 08/06/19.
 */

public class SearchProductActivity extends BaseActivity {

    private ActivitySearchProductBinding binding;

    private MercadoLivreService.SearchProductEvent searchProductEvent;
    private BindingRecyclerViewArrayAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_search_product);
        binding.searchView.setListener(new SearchProductView.SearchListener() {
            @Override
            public void clearSearch() {

            }

            @Override
            public void requestProduct(String query) {
                searchProductEvent = new MercadoLivreService.SearchProductEvent(query);
                EventBus.getDefault().post(searchProductEvent);
            }

            @Override
            public void submitProduct(String product) {

            }

            @Override
            public void onInputTouch(String prefix) {

            }

            @Override
            public void backButtonClick(boolean hideKeyboard) {
                onBackPressed();
            }
        });

        setShouldRegisterForEvents(true);

        adapter = new BindingRecyclerViewArrayAdapter<Product>(this, R.layout.item_search, BR.product);
        binding.recycler.setLayoutManager(new LinearLayoutManager(this));
        binding.recycler.setAdapter(adapter);

        adapter.setOnItemClickListener(new RecyclerViewArrayAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(View view, Object o, int i) {
                Intent intent = new Intent(SearchProductActivity.this, ProductDetailActivity.class);
                Product product = (Product) o;
                intent.putExtra(PRODUCT_ID, product.getId());
                startActivity(intent);
            }
        });
    }

    @Subscribe
    public void onEvent(MercadoLivreService.SearchProductLoadedEvent event) {
        binding.loadingView.hide();
        if (event.source == searchProductEvent) {
            if (event.product != null) {
                adapter.bindData(event.product.getResults());
            }
        }
    }

    @Subscribe
    public void onEvent(MercadoLivreService.SearchProductEventErrorEvent event) {
        binding.loadingView.hide();
        if (event.source == searchProductEvent) {
            Log.i("HERE>>", "SearchProductEventErrorEvent: " + event);
        }
    }
}
