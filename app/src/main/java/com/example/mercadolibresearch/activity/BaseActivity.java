package com.example.mercadolibresearch.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by rodrigoprimus on 08/06/19.
 */

public class BaseActivity extends AppCompatActivity {

    protected static String PRODUCT_ID = "PRODUCT_ID";
    protected static int SEARCH_PRODUCT = 100;

    private boolean shouldRegisterForEvents = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void onStart() {
        super.onStart();

        if (shouldRegisterForEvents && !EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onStop();
    }

    protected void setShouldRegisterForEvents(boolean shouldRegisterForEvents) {
        this.shouldRegisterForEvents = shouldRegisterForEvents;
    }
}
