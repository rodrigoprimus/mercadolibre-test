package com.example.mercadolibresearch.binding;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;

import com.example.mercadolibresearch.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.io.File;

/**
 * Created by rodrigoprimus on 08/06/19.
 */

public class CustomBindings {

    @BindingAdapter({"loadImage"})
    public static void loadImage(ImageView view, String url) {
        if(url != null) {
            Picasso picasso = Picasso.with(view.getContext());
            Drawable placeholder = ContextCompat.getDrawable(view.getContext(), R.drawable.transparent_placeholder);
            RequestCreator requestCreator;
            if(url.contains("http")) {
                requestCreator = picasso.load(url).placeholder(placeholder);
            } else {
                Uri uri = Uri.fromFile(new File(url));
                requestCreator = picasso.load(uri).placeholder(placeholder);
            }

            requestCreator.into(view);
        }
    }
}
