package com.example.mercadolibresearch.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by rodrigoprimus on 10/06/19.
 */

public class DimensionsHelper {

    private static Point mScreenSize;

    public static Point getScreenSize(Context context){
        if ( mScreenSize == null ){
            WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            Display display = windowManager.getDefaultDisplay();
            Point size = new Point();

            if (android.os.Build.VERSION.SDK_INT>= android.os.Build.VERSION_CODES.HONEYCOMB) {
                display.getSize(size);
            } else {
                size.x = display.getWidth();  // deprecated
                size.y = display.getHeight();  // deprecated
            }
            mScreenSize = size;
        }
        return mScreenSize;
    }

    public static float dpToPixels(float dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, Resources.getSystem().getDisplayMetrics());
    }
}
