package com.example.mercadolibresearch.utils;

import com.example.mercadolibresearch.models.Product;

import java.util.Locale;

import static java.lang.String.format;

/**
 * Created by rodrigoprimus on 11/06/19.
 */

public class ProductUtils {

    private static final Locale BR_LOCALE = new Locale("pt", "BR");

    public static String title(Product product) {
        if (product == null) return "";
        return product.getTitle();
    }

    public static String price(Product product) {
        if (product == null) return "";
        Float price = product.getPrice();
        return format(BR_LOCALE, "R$ %.02f", price);
    }

    public static String sales(Product product) {
        if (product == null) return "";
        return product.getSold_quantity() + " vendidos";
    }

    public static String thumbnail(Product product) {
        if (product == null) return null;
        return product.getThumbnail();
    }

    public static String picture(Product product) {
        if (product == null) return null;
        if (product.getPictures() == null) return null;
        return product.getPictures().get(0).getSecure_url();
    }
}
